const DataFrame = require("dataframe-js").DataFrame;
const utils = require("./utils.js");

class Predictors {

  // constructor(formula, dataDF, options = null, origPredictors = null, termsToAvoid = []) {
  //   this.options = options;
  //
  //   this.formula = formula;
  //   let tildeAt = formula.indexOf('~');
  //   let rs = formula.substring(tildeAt + 1);
  //   this.rsFormula = rs.trim();
  //   this.ls = formula.substring(0,tildeAt).trim();
  //   this.dataDF = dataDF;
  //   // Break formula into terms
  //   let weAreCretingANewModel = true;
  //   if (origPredictors) {
  //     weAreCretingANewModel = false;
  //     this.terms = origPredictors.terms.slice().splice(1);
  //     this.parsedRSFormula = origPredictors.parsedRSFormula;
  //   } else {
  //     this._setTerms(this.rsFormula, termsToAvoid);
  //   }
  //
  //   this.predDF = this.buildPredictorsDataframe(weAreCretingANewModel);
  // }

  build(formula, dataDF, options = null, origPredictors = null, termsToAvoid = []) {
    this.options = options;

    this.formula = formula;
    let tildeAt = formula.indexOf('~');
    let rs = formula.substring(tildeAt + 1);
    this.rsFormula = rs.trim();
    this.ls = formula.substring(0,tildeAt).trim();
    this.dataDF = dataDF;
    // Break formula into terms
    let weAreCretingANewModel = true;
    if (origPredictors) {
      weAreCretingANewModel = false;
      this.terms = origPredictors.terms.slice().splice(1);
      this.parsedRSFormula = origPredictors.parsedRSFormula;
    } else {
      this._setTerms(this.rsFormula, termsToAvoid);
    }

    this.predDF = this.buildPredictorsDataframe(weAreCretingANewModel);
  }

  static create(formula, dataDF, options = null, origPredictors = null, termsToAvoid = []) {
    let instance =  Object.create(this.prototype);
    instance.build(formula, dataDF, options, origPredictors, termsToAvoid);
    return instance;
  }


  _validateFormula() {
    if (this.rsFormula === '') {
      throw new Error("Missing predictors in formula");
    }

    let colNames = this.dataDF.listColumns().map(x => {return x.toUpperCase()});
    this.parsedRSFormula.subterms.forEach( term => {
      if (!colNames.includes(term.toUpperCase())) {
        throw new Error("Invalid term in formula: " + term);
      }
    });

  }

  _isInteraction(term) {
    return term.indexOf(':') > 0;
  }

  _hasAsterisk(term) {
    return term.indexOf('*') > 0;
  }

  _numberOfTermsInInteraction(term) {
    return term.split(":").length - 1;
  }

  _getInteractionSubterms(term) {
    let subterms = term.split(':');
    let res = [];
    subterms.map(x => res.push(x.trim()));
    return res;
  }

  _isFactor(term) {
    if (!this.options || !this.options.factors) {
      return false;
    }

    return this.options.factors.includes(term);
  }


  _breakInAdditionAndSubstractionTerms(rsFormula) {
    let addAndSubstractTerms = {
      additive:[],
      substraction:[]
    };

    rsFormula.split('+').forEach( term => {
      let subsOpIndex = term.indexOf('-');
      if (subsOpIndex > 0) {
        term = term.substr(0, subsOpIndex);
      }
      addAndSubstractTerms.additive.push(term);
    });

    if (rsFormula.indexOf('-') > 0) {
      rsFormula.split('-').forEach( term => {
        let addOpIndex = term.indexOf('+');
        if (addOpIndex > 0) {
          term = term.substr(0, addOpIndex);
        }
        addAndSubstractTerms.substraction.push(term);
      });
    }
    return addAndSubstractTerms;
  }

  _getParsedFormula(rsFormula) {
    let parsedFormula = {
      list:[],
      substractionTerms:[],
      props: {},
      subterms: new Set(),
      factorProperties: {}
    };

    let addAndSubTerms = this._breakInAdditionAndSubstractionTerms(rsFormula);
    addAndSubTerms.additive.forEach( term => {
      term =  term.trim();
      parsedFormula.list.push(term);
      parsedFormula.props[term] = {};
      parsedFormula.props[term].isFactor = this._isFactor(term);
      parsedFormula.props[term].isInteraction = this._isInteraction(term);
      parsedFormula.props[term].hasSubTerms = false;
      if (parsedFormula.props[term].isInteraction) {
        parsedFormula.props[term].hasSubTerms = true;
        let subterms = term.split(':');
        subterms.map(x => parsedFormula.subterms.add(x.trim()));
      } else {
        parsedFormula.subterms.add(term)
      }
    });

    addAndSubTerms.substraction.forEach( term => {
      term =  term.trim();
      parsedFormula.substractionTerms.push(term);
    });

    // Sort the terms by number of subterms in the interaction
    parsedFormula.list.sort((t1,t2) => {return (this._numberOfTermsInInteraction(t1) - this._numberOfTermsInInteraction(t2))});
    parsedFormula.factorProperties = this._getFactorProperties();

    return parsedFormula;
  }

  _getFactorProperties() {
    let properties = {};
    if (this.options && this.options.factors && this.options.factors.length > 0) {
      let factors = this.options.factors;
      for (let factorIndex in factors) {
        let factor = factors[factorIndex];
        // Find the different values for this factor.
        let distinctValuesTemp = this.dataDF.distinct(factor).toArray();
        let distinctValues = [];
        distinctValuesTemp.map(x => distinctValues.push(x[0].toString()));
        distinctValues = distinctValues.sort();
        properties[factor] = {values:distinctValues, reference:distinctValues[0] };
      }
    }
    return properties;
  }

  _getInteractionCombinationsHelper(termsList, termValues, result = []) {
    if (termsList.length === 0) {
      return result;
    }

    let term = termsList[0];
    let values = termValues[term];
    if (result.length === 0) {
      for (let k = 0; k < values.length; k++) {
        result.push(values[k]);
      }
      return this._getInteractionCombinationsHelper(termsList.slice(1), termValues, result);
    } else {
      let acc = [];
      for (let i = 0; i< result.length; i++) {
        for (let k = 0; k < values.length; k++) {
          acc.push(result[i] +':' + values[k]);
        }
      }
      return this._getInteractionCombinationsHelper(termsList.slice(1), termValues, acc);
    }
  }


  _getInteractionCombinations(termsList, factorProps) {
    let termValues = {};
    termsList.forEach( term => {
      let isFactor = this._isFactor(term);
      termValues[term] = [];
      if (!isFactor) {
        termValues[term].push(term);
      } else {
        let factorVals = factorProps[term].values;
        factorVals.map(fv => termValues[term].push(term + fv));
      }
    });
    return this._getInteractionCombinationsHelper(termsList, termValues);
  }


  _getAsteriskSubtermsHelper(acc, terms) {
    if (terms.length === 0) {
      return acc;
    }
    let newTerms = [];
    for (let i = 0; i<acc.length; i++) {
      for (let j = 0; j < terms.length; j++) {
        let newTerm = acc[i] + ':' + terms[j];
        newTerms = newTerms.concat(this._getAsteriskSubtermsHelper([newTerm], terms.slice(j + 1)))
      }
    }
    return acc.concat(newTerms);
  }

  _getAsteriskSubterms(arr) {
    let comb = [];
    for (let i=0; i < arr.length; i++) {
      let newTerms = this._getAsteriskSubtermsHelper([arr[i]], arr.slice(i+1));
      comb = comb.concat(newTerms);
    }
    return comb;
  }



  _parseTermsWithAsterisks(rsFormula) {
    if (rsFormula.indexOf('*') < 0 ){
      return rsFormula;
    }
    let replacement = '';
    let terms = rsFormula.split('+');
    terms.map(term => {
      if (this._hasAsterisk(term)){
        let subTerms = term.split('*');
        subTerms.map(Function.prototype.call, String.prototype.trim);
        let combs = this._getAsteriskSubterms(subTerms);
        combs.map(x => replacement = replacement + ' + ' + x);
      } else {
        replacement = replacement + ' + ' + term;
      }
    });
    if (replacement.startsWith(' +')) {
      replacement = replacement.substr(3);
    }
    return replacement;
  }

  _setTerms(rsFormula, termsToAvoid = []) {
    // If the rightside formula is a dot, consider all the predictors that are not target
    if (rsFormula === '.') {
      let colNames = this.dataDF.listColumns();
      rsFormula = '';
      colNames.forEach( colName => {
        if (colName !== this.ls) {
          rsFormula = rsFormula + colName + '+';
        }
      });

      if (rsFormula.length > 1) {
        rsFormula = rsFormula.substr(0, rsFormula.length - 1);
      } else {
        // TODO: Something went wrong
      }
    }

    // Parse terms with asterisk
    rsFormula = this._parseTermsWithAsterisks(rsFormula);

    // Parse the RS formula
    this.parsedRSFormula = this._getParsedFormula(rsFormula);
    this._validateFormula();
    let factorsProps = this.parsedRSFormula.factorProperties;
    let tempTerms = [];
    this.parsedRSFormula.list.forEach( term => {
      let termProps = this.parsedRSFormula.props[term];
      //Avoid duplicate terms
      if (!tempTerms.includes(term) && !this.parsedRSFormula.substractionTerms.includes(term)) {
        if (termProps.isFactor && factorsProps[term].values.length > 1) {
          let factorVals = factorsProps[term].values.slice(1);
          factorVals.map(fv => tempTerms.push(term + fv));
        } else if (termProps.isInteraction) {
          let termComb = this._getInteractionCombinations(this._getInteractionSubterms(term), factorsProps);
          termComb.map(t => {
            if (!this.parsedRSFormula.substractionTerms.includes(t)){
              tempTerms.push(t);
            }
          });
        } else {
          tempTerms.push(term);
        }
      }
    });

    //Remove the terms to avoid from the terms array:
    this.terms = [];
    tempTerms.forEach(t => {
      if (!termsToAvoid.includes(t)) {
        this.terms.push(t);
      }
    });

  }

  _buildSubTermsObject() {
    // Add all the factor related columns with zero values:
    let predDF =  {};
    let factorsProps = this.parsedRSFormula.factorProperties;
    let rowsNum = this.dataDF.dim()[0];
    let subterms = Array.from(this.parsedRSFormula.subterms);
    predDF['(inter.)'] = new Array(rowsNum).fill(1);
    subterms.forEach( term => {
      let termProps = factorsProps[term];
      if (this._isFactor(term) === false) {
        predDF[term] = this.dataDF.select(term).toDict()[term];
      } else {
        termProps.values.forEach(factorVal => {
          let colName = term + factorVal;
          predDF[colName] = new Array(rowsNum).fill(0);
        });
      }
    });

    // Set the values for factor columns
    let dataDFDict = this.dataDF.toDict();
    // Loop through the terms
    for (let termIndex=0; termIndex<subterms.length; termIndex++ ) {
      let term = subterms[termIndex];
      // If this one is a factor...
      if (this._isFactor(term) === true) {
        // Loop through the rows to set a 1 when necessary
        for (let idx = 0; idx < rowsNum; idx++){
          let val = dataDFDict[term][idx];
          predDF[term + val][idx] = 1;
        }
      }
    }
    return predDF;
  }


  _calculateInteraction(intTerm, subtermsObj) {
    let terms = this._getInteractionSubterms(intTerm);
    let res = subtermsObj[terms[0]].slice(0);
    let arrLength = res.length;
    terms.slice(1).forEach( term => {
      for(let k=0; k < arrLength; k++) {
        res[k] = res[k] * subtermsObj[term][k];
      }
    });
    return res;
  }

  buildPredictorsDataframe(toCreateModel = true) {
    let subTerms = this._buildSubTermsObject();
    let predDF = {};

    predDF['(inter.)'] = subTerms['(inter.)'];
    // Loop through the terms to build predDF:
    for (let termIndex=0; termIndex < this.terms.length; termIndex++) {
      let term = this.terms[termIndex];
      //let termProps = this.parsedRSFormula.props[term];
      if (this._isInteraction(term)) {
        predDF[term] = this._calculateInteraction(term, subTerms);
      } else {
        predDF[term] = subTerms[term];
      }
    }

    if (toCreateModel) {
      // Remove constant columns
      let itemsToRemove = [];
      for (let termIndex=0; termIndex < this.terms.length; termIndex++) {
        let term = this.terms[termIndex];
        let val1 = predDF[term][0];
        let constantValues = true;
        for (let valNum=1; valNum < predDF[term].length ;valNum++) {
          if (val1 !== predDF[term][valNum]) {
            constantValues = false;
            break;
          }
        }
        if (constantValues) {
          itemsToRemove.push(term);
        }
      }
      for (let termIndex=0; termIndex < itemsToRemove.length; termIndex++) {
        let term = itemsToRemove[termIndex];
        this.terms = utils.removeItemFromArray(this.terms, term);
        delete predDF[term];
      }
    }



    this.terms.unshift('(inter.)');
    predDF = new DataFrame(predDF , this.terms);
    return predDF;
  }

  getPredDF() {
    return this.predDF;
  }

  getNumberOfCoeff() {
    return this.terms.length
  }

  getTerms() {
    return this.terms;
  }
}

module.exports =  Predictors;