class Target {
  build(formula, df) {
    let tildeAt = formula.indexOf('~');
    let ls = formula.substring(0,tildeAt);
    this.formula = formula;
    this.lsFormula = ls.trim();
    this._validateFormula(df);
    this.y = df.select(this.lsFormula);
  }

  static create(formula, df) {
    let instance =  Object.create(this.prototype);
    instance.build(formula, df);
    return instance;
  }
  _validateFormula(df) {
    if (this.lsFormula === '') {
      throw new Error("Missing target in formula");
    }

    let colNames = df.listColumns().map(x => {return x.toUpperCase()});
    if (!colNames.includes(this.lsFormula.toUpperCase())) {
      throw new Error("Invalid target in formula");
    }
  }


  getYCol() {
    return this.y;
  }
}

module.exports = Target;