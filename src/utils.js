class Utils {
  constructor() { }


  expo(x, f) {
    return Number.parseFloat(x).toExponential(f);
  }

  removeItemFromArray(items, valueToRemove) {
    const filteredItems = items.filter(item => item !== valueToRemove);
    return filteredItems;
  }


}

module.exports =  new Utils();