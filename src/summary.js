
const verySmallNumber = Math.pow(0.2, 16);


class Summary{
  build(formula, resStatsDF, coeffDF, residualsDF, r_squared, adj_r_squared, degFreedom, sigma, F, pValue){
    this.formula = formula;
    this.residuals = resStatsDF;
    this.coefficients = coeffDF;
    this.call = "lm(" + this.formula + ")";
    this.r_squared = r_squared;
    this.adj_r_squared = adj_r_squared;
    this.degFreedom = degFreedom;
    this.sigma = sigma;
    this.F = F;
    this.pValue = pValue;
  }

  static create(formula, resStatsDF, coeffDF, residualsDF, r_squared, adj_r_squared, degFreedom, sigma, F, pValue) {
    let instance =  Object.create(this.prototype);
    instance.build(formula, resStatsDF, coeffDF, residualsDF, r_squared, adj_r_squared, degFreedom, sigma, F, pValue);
    return instance;
  }

  toString(maxColLength = 5){
    let p_minus_1 = this.coefficients.dim()[0] - 1;
    let pValueStr = this.pValue > verySmallNumber?this.pValue.toString():'< 2.2e-16';
    let str = "Call: " + "\n";
    str = str + this.call +"\n\n";
    str = str + "Residuals:" + "\n";
    str = str + this.residuals.toString(maxColLength) +"\n\n";
    str = str + "Coefficients: " + "\n";
    str = str + this.coefficients.toString(maxColLength, {maxColLength:[30]});
    str = str + "\n" + "--- " + "\n";
    str = str + "Residual standard error: " + this.sigma.toFixed(5) + " on " + this.degFreedom + " degrees of freedom" + "\n";
    str = str + "Multiple R-squared: " +  this.r_squared.toFixed(5) + ",	Adjusted R-squared: " + this.adj_r_squared.toFixed(5) + "\n";
    str = str + "F-statistic: " + this.F.toFixed(5) + " on " + p_minus_1 + " and " + this.degFreedom  + " DF,  p-value: " + pValueStr + "\n";

    return str;
  };
}

module.exports = Summary;