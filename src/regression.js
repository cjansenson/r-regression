const DataFrame = require("dataframe-js").DataFrame;
const utils = require("./utils.js");
const verySmallNumber = Math.pow(0.2, 16);
const Model = require('./model.js');

Object.assign(DataFrame.prototype, {
  toString(maxColLength=5, options = {}, expFormat = 'AS_REQUIRED') {
     if (maxColLength < 5) {
       maxColLength = 5;
     }

     let makeRow = function makeRow(row) {
      return "| " + row.map(function (column, idx) {
        let mcl = maxColLength;
        if (options && options.maxColLength) {
            mcl = options.maxColLength[idx]?options.maxColLength[idx]:maxColLength;
        }

        if (mcl > 0) {
          let columnAsString = String(column);
          if (Number(column) === column ) {
            if (Math.abs(column) > 0 && Math.abs(column) < verySmallNumber) {
              return '<2e-16'.padEnd(mcl + 3);
            }

            if (expFormat === 'ALL' || Math.abs(column) < Math.pow(0.1, mcl - 1)) {
              columnAsString = utils.expo(column, mcl - 2).toString();
            } else {
              columnAsString = columnAsString.length > (mcl - 1) ? columnAsString.substring(0, mcl + 2) + "" : columnAsString + Array(mcl - columnAsString.length).join(" ");
            }
            columnAsString = columnAsString.padEnd(mcl + 3);
            return columnAsString;
          } else {
             columnAsString = String(column);
             columnAsString = columnAsString.length > (mcl - 1) ? columnAsString.substring(0, mcl + 2) + "" : columnAsString + Array(mcl - columnAsString.length).join(" ");
             return columnAsString.padEnd(mcl + 3);
          }

        } else {
          return String(column);
        }
      }).join(" | ") + " |";
    };

    let header = makeRow(this.listColumns());
    let toShow = header + "\n" + Array(header.length).join("-") + "\n" ;
    let rows = this.toCollection(true);
    for (let rowIndex in rows) {
      toShow = toShow + makeRow(rows[rowIndex].toArray() ) + ("\n");
    }

    return toShow;
  },

  setArrValues() {
    this.mat = this.toArray();
  },

});

class Regression {
  constructor(formula, dataDF) {
    this.formula = formula;
    this.dataDF = dataDF;
  }


  // lm(mpg ~ wt + year, data = autompg)
  // lm(mpg ~ ., data = autompg)
  // lm(mpg ~ 1, data = autompg)
  // lm(mpg ~ disp * domestic, data = autompg)
  // lm(formula = mpg ~ disp + domestic + disp:domestic, data = autompg)
  // lm(y ~ 0 + v1 + v2 + v3 + x:v1 + x:v2 + x:v3, data = new_param_data)
  // lm(mpg ~ disp * hp * domestic, data = autompg)
  // lm(mpg ~ disp * hp + disp * domestic + hp * domestic, data = autompg)
  // lm(log(salary) ~ years, data = initech)
  // lm(formula = log(salary) ~ years, data = initech)
  // lm((((Species ^ 0.3) - 1) / 0.3) ~ Area + Elevation + Nearest + Scruz + Adjacent, data
  // lm(log(mpg) ~ log(hp), data = autompg)
  // lm(sales ~ advert + I(advert ^ 2), data = marketing)
  // lm(sales ~ advert + I(advert ^ 2) + I(advert ^ 3), data = marketing)
  // lm(mpg ~ poly(mph, 6), data = econ)
  // lm(y ~ poly(x, 4), data = data_higher)
  // lm(HtShoes ~ . - hipcenter, data = seatpos)
  // predict(mpg_model, newdata = new_cars, interval = "confidence", level = 0.99)
  // resid(stop_dist_model)
  // summary(stop_dist_model)
  // coef(stop_dist_model)
  // anova(stop_dist_model)
  // Dataframe API: https://gmousse.gitbooks.io/dataframe-js/content/doc/api/dataframe.html
  // https://vincentarelbundock.github.io/Rdatasets/datasets.html
  // Datasets: https://vincentarelbundock.github.io/Rdatasets/datasets.html

  // https://gmousse.gitbooks.io/dataframe-js/content/doc/api/dataframe.html
  // https://www.npmjs.com/package/ml-matrix
  static lm(formula, data, options = {removeNA: true}) {
    data = new DataFrame(data);
    options = options || {};
    options.removeNA = options.removeNA !== undefined ? options.removeNA:true;
    options.removeCollinearTerms = options.removeCollinearTerms !== undefined ? options.removeCollinearTerms:true;
    options.factors = options.factors ? options.factors:[];
    return Model.create(formula, data, options);
  }

  static residuals(model) {
    return model.residuals;
  }


}

module.exports = Regression;