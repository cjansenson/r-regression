const Predictors = require('./predictors.js');
const Target = require('./target.js');
const Summary = require('./summary.js');
const { Matrix } = require('ml-matrix');
const utils = require("./utils.js");
const DataFrame = require("dataframe-js").DataFrame;
const distributions = require('distributions');
const jsstats = require('js-stats');

let mergeRows = function(df, selectedColumns = []) {
  let makeRow = function makeRow(row) {
    return row.map(function (column) {
      return String(column);
    }).join(" ");
  };

  let dfCopy = df;
  if (selectedColumns.length > 0) {
    dfCopy = df.select(...selectedColumns)
  }

  let dfRows = dfCopy.toCollection(true);
  let rowsArray = [];
  for (let rowIndex in dfRows) {
    rowsArray.push(makeRow(dfRows[rowIndex].toArray()));
  }
  return rowsArray;
};

class Model {

  static create(formula, data, options = null) {
    let instance =  Object.create(this.prototype);
    instance.build(formula, data, options);
    return instance;
  }

  build(formula, data, options = null) {
    this.options = options;
    this.formula = formula;
    this.data = data;
    this.call = "lm(" + this.formula + ")";
    this._preProcessData();
    this._buildRegressionModel(this.formula, this.data, this.options);
    this._buildResidualStatsDF();
    this._buildSummary();
    this._buildModel();
  }

  _calculateQuant(valuesArray) {
    let valuesList = valuesArray.slice(0);
    valuesList.sort(function(a, b){return a-b});
    let quantiles = [25, 50, 75];
    let res = [];
    res.push(valuesList[0]);
    let result;
    for (let perc in quantiles) {
      let index = (quantiles[perc] / 100) * valuesList.length;
      if (Math.floor(index) === index) {
        result = (valuesList[(index - 1)] + valuesList[index]) / 2;
      }
      else {
        result = valuesList[Math.floor(index)];
      }
      res.push(result);
    }
    res.push(valuesList[valuesList.length - 1]);
    return res;
  }


  _getSxSx(M) {
    let sxsxValues = [];
    for (let j = 0; j<M.columns; j++) {
      let colValues = M.getColumn(j);
      let avg = colValues.reduce((a, b) => a + b) / colValues.length;
      let res = 0;
      colValues.map(x => res = res + Math.pow(x - avg, 2));
      sxsxValues.push(res);
    }
    return sxsxValues;
  }

  _filterColumnsDF(df, nColsToInclude, colIndexToExclude = []) {
    let colNames = df.listColumns();
    let colsToInclude = colNames.slice(0,nColsToInclude);
    colIndexToExclude.forEach( index => {
      let colName = colNames[index];
      colsToInclude = utils.removeItemFromArray(colsToInclude, colName);
    });
    return df.select(...colsToInclude);
  }

  _filterMatrixColumns(M, nColsToInclude, colIndexToExclude = []) {
    let numOfColumns = M.columns;
    let cols = Array.from(Array(numOfColumns).keys());
    let colsToInclude = cols.slice(0,nColsToInclude);
    colIndexToExclude.forEach( index => {
      colsToInclude = utils.removeItemFromArray(colsToInclude, index);
    });
    return M.columnSelectionView(colsToInclude);
  }

  _getStrongCollineariltyTerms(predictors) {
    let res = [];
    let collinearPred = [];
    let colIndex = 2;
    let X = predictors.getPredDF();
    let colNames = X.listColumns();
    let colsNumber = colNames.length;
    let M = new Matrix(X.toArray());
    let sxSxValues = this._getSxSx(M);
    while (colIndex <= colsNumber) {
      let MFiltered = this._filterMatrixColumns(M, colIndex, collinearPred);
      let M_T = MFiltered.transpose();
      let C = M_T.mmul(MFiltered).pseudoInverse();
      let diag = C.diag();
      let vifArray = [];

      diag.forEach((Cjj, index) => {
        let SxjSxj = sxSxValues[index];
        vifArray.push(Math.abs(Cjj * SxjSxj));
      });

      // If the vifArray contains a large number, remove:
      if (Math.max(...vifArray) >= 1000 ) {
        collinearPred.push(colIndex - 1);
        res.push(colNames[colIndex - 1])
      }
      colIndex = colIndex + 1;
    }
    return res;
  }

  _preProcessData() {
    // Remove rows with NA values
    if (this.options.removeNA){
      let filteredData = this.data.where(row =>
        !row.toArray().includes('NAN') && !row.toArray().includes('NA')
      );
      this.data = filteredData;
    }

    if (!this.options.dropInvalidColumns && !this.options.automateFactors) {
      return;
    }
    // Exclude columns with non-numeric data for non-factor columns, throw error
    let colNames = this.data.listColumns();
    let dataObject = this.data.toDict();
    colNames.map( name => {
        if (!this.options.factors.includes(name)) {
          let numeric = dataObject[name].reduce((acc, x) => acc && (typeof x === 'number'), true);
          if (!numeric) {
            if (this.options.automateFactors) {
              this.options.factors.push(name);
            } else {
              console.log('Warning, dropping column "' + name + '" because it contains non numeric data.');
              this.data = this.data.drop(name);
            }
          }
        }
    });
  }

  _getFStatsProb(df1, df2, f) {
    let f_distribution = new jsstats.FDistribution(df1, df2);
    return f_distribution.cumulativeProbability(f);
  }
  _buildRegressionModel(formula, data, options) {

    let tildeAt = formula.indexOf('~');
    if (tildeAt < 0) {
      throw new Error("Invalid formula - missing '~'");
    }

    this.data = data;
    let target = Target.create(formula, data);
    let yCol = target.getYCol();

    this.predictors = Predictors.create(formula, data, options, null);
    let strongColTerms = [];
    if (options.removeCollinearTerms) {
       strongColTerms = this._getStrongCollineariltyTerms(this.predictors);
    }
    if (strongColTerms.length > 0) {
      this.predictors = Predictors.create(formula, data, options, null, strongColTerms);
    }

    let X = this.predictors.getPredDF();
    this.rowsNum = data.dim()[0];
    let M = new Matrix(X.toArray());
    let M_T = M.transpose();
    this.M_T_M_inv = M_T.mmul(M).pseudoInverse();

    let yArray = new Matrix(yCol.toArray()).to1DArray();
    let y = new Matrix(yCol.toArray());
    this.B = this.M_T_M_inv.mmul(M_T).mmul(y);

    // Coefficients
    this.coefficientsArray = this.B.to1DArray();
    this.coefficients = {};
    Array.from(Array(this.predictors.terms.length).keys()).map(
      index => this.coefficients[this.predictors.terms[index]] = this.coefficientsArray[index]);

    this.fittedValuesMat = M.mmul(this.B);
    this.fittedValues = this.fittedValuesMat.to1DArray();
    // Residuals
    this.residualValues = Matrix.sub(y, this.fittedValuesMat);
    this.residuals = this.residualValues.to1DArray();
    //this.resArray = this.residualValues.to1DArray();

    // Quantiles
    this.resStats = this._calculateQuant(this.residuals);
    // console.log(quant);

    let p = this.predictors.getNumberOfCoeff();
    this.degFreedom = this.rowsNum - p;
    // SE or SIGMA or Residual Standard Error (RSE):
    this.se = Math.sqrt(this.residualValues.transpose().mmul(this.residualValues) / (this.rowsNum - p));

    // Coeff SE:
    let coeffStdError = [];
    for (let i=0; i < p; i++) {
      coeffStdError.push(this.se * Math.sqrt(this.M_T_M_inv[i][i]));
    }

    this.mean_y = 0;
    yArray.map(x => this.mean_y = this.mean_y + x);
    this.mean_y = this.mean_y / yArray.length;

    // Sum of Square Errors
    this._SSE  = 0;
    this.residuals.map(e => this._SSE = this._SSE + e * e);

    // Sum of Squares Total
    this._SST  = 0;
    yArray.map(y_i => this._SST = this._SST + (y_i - this.mean_y) * (y_i - this.mean_y));

    this._MSE = this._SSE / this.degFreedom;

    let SSReg = this._SST - this._SSE;
    this._MSReg = SSReg / (p - 1);

    this.F = this._MSReg/this._MSE;
    this.pValue = 1 - this._getFStatsProb(p - 1, this.degFreedom, this.F);

    // R Squared and Adj R squared
    this.r_squared = 1 - (this._SSE/this._SST);
    this.adj_r_squared = 1 - (this.rowsNum - 1)/(this.rowsNum - p) * (1 - this.r_squared);
    //
    // T values:
    let tValues = [];
    for (let i=0; i < p; i++) {
      tValues.push(this.coefficientsArray[i] / (this.se * Math.sqrt(this.M_T_M_inv[i][i])));
    }

    // P(>|t|)
    let studentt = distributions.Studentt(this.degFreedom);
    //let t_distribution = new jsstats.TDistribution(this.degFreedom);

    let ptValues = [];
    for (let i=0; i < p; i++) {
      ptValues.push(2*(studentt.cdf(-Math.abs(tValues[i]))));
     // ptValues.push(2*(t_distribution.cumulativeProbability(-Math.abs(tValues[i]))));
    }


    this.coeffDF =  new DataFrame({
      ' ': this.predictors.getTerms(),
      Estimate: this.B.to1DArray(),
      'Std. Error': coeffStdError,
      't value': tValues,
      'Pr(>|t|)': ptValues
    }, ['Name', 'Estimate', 'Std. Error', 't value', 'Pr(>|t|)']);
    this.coeffDF.setArrValues();
  }

  _buildResidualStatsDF() {
    this.resStatsDF = new DataFrame(
      [this.resStats], ['Min', '1Q', 'Median','3Q','Max']);
    this.resStatsDF.setArrValues();
  }

  _buildModel() {
    let mergedValues = mergeRows(this.data);
    this.model = new DataFrame({'': mergedValues, 'res': this.residualValues}, ['', 'res']);
  }

  predict(newDF, interval = null, level = .95) {
    //Sort the columns accordingly
    newDF = new DataFrame(newDF);
    let newDataPredictors = Predictors.create(this.formula, newDF, this.options, this.predictors);
    let newX = newDataPredictors.getPredDF();
    let newM = new Matrix(newX.toArray());
    let fit = newM.mmul(this.B).to1DArray();
    if (!interval) {
      return fit;
    }

    if (interval === 'confidence' || interval === 'prediction') {
      let newM_T = newM.transpose();
      let studentt = distributions.Studentt(this.degFreedom);
      let t = studentt.inv(level + (1 - level)/2);
      let margins = [];

      let temp  = newM.mmul(this.M_T_M_inv).mmul(newM_T);
      temp  = temp.diag();
      if (interval === 'confidence'){
        temp.map(x => margins.push(t * this.se * Math.sqrt(x)));
      } else {
        temp.map(x => margins.push(t * this.se * Math.sqrt(1 + x)));
      }

      let upr = [];
      margins.map((x, idx) => upr.push( fit[idx] + x));
      let lwr = [];
      margins.map((x, idx) => lwr.push( fit[idx] - x));

      let predictDF =  new DataFrame({
        'fit': fit,
        'lwr': lwr,
        'upr': upr
      }, ['fit', 'lwr', 'upr']);
      predictDF.setArrValues();
      return predictDF;
    }

  }

  _buildSummary() {
    this.summary = Summary.create(this.formula, this.resStatsDF, this.coeffDF, this.residuals,
      this.r_squared, this.adj_r_squared, this.degFreedom, this.se, this.F, this.pValue);
  }

}

module.exports =  Model;