const expect = require('chai').expect,
  assert = require('chai').assert,
  regression = require('../index'),
  csvdata = require('csvdata');


const DataFrame = require("dataframe-js").DataFrame;
/**
 *Comments here
 */
describe('factors_regression_example', function () {
  // beforeEach(function () {
  //   // DO something?
  // });

  it('create_model_with_factors', function (done) {
    csvdata.load('./data/mpg.csv').then(df => {
      let options = {
        factors: ['cyl']
      };
      this.model = regression.lm('mpg ~ cyl + wt', df, options);
      done();
    })
  });

  it('factors_test_model_summary', function (done) {
    let summary = this.model.summary;
    expect(summary).to.not.be.null;
    console.log(summary.toString());
    done();
  });

  it('factors_test_model_summary_call', function (done) {
    let summary = this.model.summary;
    expect(summary.call.toString()).to.be.equal('lm(mpg ~ cyl + wt)');
    done();
  });

  it('factors_test_model_summary_coefficients', function (done) {
    let coef = this.model.summary.coefficients;
    expect(coef).to.not.be.null;
    expect(coef.mat).to.not.be.null;
    expect(coef.mat[1][0]).to.equal('cyl4');
    expect(coef.dim()[0]).to.equal(6);
    expect(coef.dim()[1]).to.equal(5);
    expect(coef.mat[1][1]).to.equal(8.162129195075472);
    //console.log(this.model.summary.coefficients.toString());
    done();
  });

  it('factors_test_model_summary_residuals', function (done) {
    let res = this.model.summary.residuals;
    expect(res).to.not.be.null;
    expect(res.mat).to.not.be.null;
    // expect(coef.mat[1][0]).to.equal('cyl');
    expect(res.dim()[0]).to.equal(1);
    expect(res.dim()[1]).to.equal(5);
    expect(res.mat[0][0]).to.equal(-10.252782030976697);
    expect(res.mat[0][1]).to.equal(-2.5394149722402126);
    expect(res.mat[0][2]).to.equal(-0.23263291557675458);
    expect(res.mat[0][3]).to.equal(1.9186383111989969);

    //console.log(this.model.summary.coefficients.toString());
    done();
  });

  it('factors_test_model_coefficients', function (done) {

    let coef = this.model.coefficients;
    expect(coef).to.not.be.null;
    expect(coef.cyl4).to.equal(8.162129195075472);
    expect(coef.cyl6).to.equal(4.332869020468717);
    expect(coef.cyl8).to.equal(4.8976081289709885);
    done();
  });

  it('factors_test_model_residuals', function (done) {

    let res = this.model.residuals;
    expect(res).to.not.be.null;
    expect(res[0]).to.equal(-0.6940295005174697);
    done();
  });

  it('factors_test_model_fitted_values', function (done) {

    let fitted_values = this.model.fittedValues;
    expect(fitted_values).to.not.equal(undefined);
    expect(fitted_values[0]).to.equal(18.69402950051747);
    done();
  });

  it('factors_test_model_call', function (done) {
    expect(this.model.call).to.be.equal('lm(mpg ~ cyl + wt)');
    done();
  });

  it('factors_test_model_summary_r_squared', function (done) {

    let r_squared = this.model.r_squared;
    expect(r_squared).to.not.equal(undefined);
    expect(r_squared).to.be.equal(0.7243400919394647);
    done();
  });

  it('factors_test_model_summary_adj_r_squared', function (done) {

    let adj_r_squared = this.model.adj_r_squared;
    expect(adj_r_squared).to.not.equal(undefined);
    expect(adj_r_squared).to.be.equal(0.7207507702199265);

    done();
  });

  it('factors_test_model_summary_sigma', function (done) {

    let sigma = this.model.summary.sigma;
    expect(sigma).to.not.equal(undefined);
    expect(sigma).to.be.equal(4.1300916462957);
    done();
  });

  it('factors_test_model_summary_fstatistic', function (done) {

    let fstats = this.model.fstatistics;
    //expect(fstats).to.not.equal(undefined);

    done();
  });

  it('factors_test_predict_basic', function (done) {

    const newValues = new DataFrame([
      {cyl: 8, wt: 3500},
      {cyl: 6, wt: 2000}
    ], ['cyl', 'wt']);

    let fit = this.model.predict(newValues);
    expect(fit).to.not.equal(undefined);
    expect(fit[0]).to.be.equal(18.71846578683022);
    expect(fit[1]).to.be.equal(27.31733404560777);
    done();
  });

  it('factors_test_predict_confidence', function (done) {

    const newValues = new DataFrame([
      {cyl: 8, wt: 3500},
      {cyl: 6, wt: 2000},
      {cyl: 6, wt: 3500},
      {cyl: 4, wt: 3500},
      {cyl: 3, wt: 3500}
    ], ['cyl', 'wt']);

    let fit = this.model.predict(newValues, 'confidence', .99);
    //fit.show();
    expect(fit).to.not.equal(undefined);
    expect(fit.mat[0][0]).to.be.equal(18.71846578683022);
    expect(fit.mat[0][1]).to.be.equal(17.33279201222907);
    expect(fit.mat[0][2]).to.be.equal(20.104139561431367);
    expect(fit.mat[1][0]).to.be.equal(27.31733404560777);
    expect(fit.mat[1][1]).to.be.equal(25.20168516700705);
    expect(fit.mat[1][2]).to.be.equal(29.43298292420849);

    done();
  });

  it('factors_test_predict_prediction', function (done) {

    const newValues = new DataFrame([
      {cyl: 8, wt: 3500},
      {cyl: 6, wt: 2000},
      {cyl: 6, wt: 3500},
      {cyl: 4, wt: 3500},
      {cyl: 3, wt: 3500}
    ], ['cyl', 'wt']);

    let fit = this.model.predict(newValues, 'prediction', .99);
    //fit.show();
    expect(fit).to.not.equal(undefined);
    expect(fit.mat[0][0]).to.be.equal(18.71846578683022);
    expect(fit.mat[0][1]).to.be.equal(7.937505715197151);
    expect(fit.mat[0][2]).to.be.equal(29.499425858463287);
    expect(fit.mat[1][0]).to.be.equal(27.31733404560777);
    expect(fit.mat[1][1]).to.be.equal(16.418481884299517);
    expect(fit.mat[1][2]).to.be.equal(38.21618620691602);

    done();
  });

});
