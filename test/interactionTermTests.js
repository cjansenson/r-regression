const expect = require('chai').expect,
  assert = require('chai').assert,
  regression = require('../index'),
  csvdata = require('csvdata');


const DataFrame = require("dataframe-js").DataFrame;
/**
 *Comments here
 */
describe('factor_interaction', function () {

  it('create_factor_factor_interaction_model', function (done) {
    this.timeout(10000);
    csvdata.load('./data/epa2015.csv').then(df => {
      let df1 = new DataFrame(df);
      let options = {
        factors: ['type', 'drive', 'lockup']
      };
      this.model = regression.lm('CO ~ type:lockup', df1, options);
      done();
    })
  });

  it('interaction_basic_model_coefficients', function (done) {
    let summary = this.model.summary;
    expect(summary).to.not.be.null;
    console.log(summary.toString(10));
    done();
  });


  it('test_predict_confidence', function (done) {

    const newValues = new DataFrame([
      {type: 'Car', lockup: 'Y'},
      {type: 'Car', lockup: 'N'},
      {type: 'Truck', lockup: 'Y'}
    ], ['type', 'lockup']);

    let fit = this.model.predict(newValues, 'confidence', .99);
    //fit.show();
    expect(fit).to.not.equal(undefined);
    expect(fit.mat[0][0]).to.be.equal(0.30359171625198605);
    expect(fit.mat[0][1]).to.be.equal(0.27551267732139756);
    expect(fit.mat[0][2]).to.be.equal(0.33167075518257455);
    expect(fit.mat[1][0]).to.be.equal(0.35991169892984604);
    expect(fit.mat[1][1]).to.be.equal(0.3178293260648245);
    expect(fit.mat[1][2]).to.be.equal(0.40199407179486757);

    done();
  });

  it('test_predict_prediction', function (done) {

    const newValues = new DataFrame([
      {type: 'Car', lockup: 'Y'},
      {type: 'Car', lockup: 'N'},
      {type: 'Truck', lockup: 'Y'}
    ], ['type', 'lockup']);

    let fit = this.model.predict(newValues, 'prediction', .99);
    //fit.show();
    expect(fit).to.not.equal(undefined);
    expect(fit.mat[0][0]).to.be.equal(0.30359171625198605);
    expect(fit.mat[0][1]).to.be.equal(-0.9171200791902478);
    expect(fit.mat[0][2]).to.be.equal(1.52430351169422);
    expect(fit.mat[1][0]).to.be.equal(0.35991169892984604);
    expect(fit.mat[1][1]).to.be.equal(-0.861202456753597);
    expect(fit.mat[1][2]).to.be.equal(1.5810258546132891);

    done();

  });

  it('create_complex_interaction_model1', function (done) {
    this.timeout(10000);
    csvdata.load('./data/epa2015.csv').then(df => {
      let df1 = new DataFrame(df);
      let options = {
        factors: ['type', 'drive', 'lockup']
      };
      this.model = regression.lm('CO ~ type:lockup + weight', df1, options);
      done();
    })
  });

  it('interaction_interactive_model_coefficients1', function (done) {
    let summary = this.model.summary;
    expect(summary).to.not.be.null;
    let coef = summary.coefficients;
    expect(coef).to.not.be.null;
    expect(coef.mat).to.not.be.null;
    expect(coef.dim()[0]).to.equal(7);
    expect(coef.dim()[1]).to.equal(5);
    expect(coef.mat[0][1]).to.equal(-0.12981101192099634);
    expect(coef.mat[0][2]).to.equal(0.05794115922706769);
    expect(coef.mat[0][3]).to.equal(-2.2403937658940394);
    expect(coef.mat[0][4]).to.equal(0.02511503971403515);
    expect(coef.mat[0][1]).to.equal(-0.12981101192099634);

    done();
  });

  it('create_factor_factor_plus_factor_interaction_model2', function (done) {
    csvdata.load('./data/epa2015.csv').then(df => {
      let df1 = new DataFrame(df);
      let options = {
        factors: ['type', 'drive', 'lockup']
      };
      this.model = regression.lm('CO ~ lockup + type:lockup', df1, options);
      done();
    })
  });


  it('interaction_interactive_model_coefficients2', function (done) {
    let summary = this.model.summary;
    expect(summary).to.not.be.null;
    console.log(summary.toString(10));
    let coef = summary.coefficients;
    // expect(coef).to.not.be.null;
    // expect(coef.mat).to.not.be.null;
    // expect(coef.dim()[0]).to.equal(7);
    // expect(coef.dim()[1]).to.equal(5);
    // expect(coef.mat[0][1]).to.equal(-0.1298110119035314);
    // expect(coef.mat[0][2]).to.equal(0.057941159222294925);
    // expect(coef.mat[0][3]).to.equal(-2.240393765777161);
    // expect(coef.mat[0][4]).to.equal(0.025115039721667107);
    // expect(coef.mat[0][1]).to.equal(-0.1298110119035314);

    done();
  });

});
