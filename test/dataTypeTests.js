const expect = require('chai').expect,
  assert = require('chai').assert,
  regression = require('../index'),
  csvdata = require('csvdata');


const DataFrame = require("dataframe-js").DataFrame;
/**
 *Comments here
 */
describe('test_different_types', function () {

  it('test_using_a_collection_of_objects', function (done) {
    csvdata.load('./data/mpg.csv').then(df => {
      this.model = regression.lm('mpg ~ cyl + disp + hp', df);
      let summary = this.model.summary;
      expect(summary).to.not.be.null;
      done();
    })
  });

  it('test_using_a_dictionary_of_arrays', function (done) {
    csvdata.load('./data/mpg.csv').then(df => {
      let df1 = new DataFrame(df);
      this.model = regression.lm('mpg ~ cyl + disp + hp', df1.toDict());
      let summary = this.model.summary;
      expect(summary).to.not.be.null;
      done();
    })
  });

  it('test_using_a_dictionary_of_arrays', function (done) {
    csvdata.load('./data/mpg.csv').then(df => {
      let df1 = new DataFrame(df);
      this.model = regression.lm('mpg ~ cyl + disp + hp', df1.toCollection());
      let summary = this.model.summary;
      expect(summary).to.not.be.null;
      done();
    })
  });

});
