const expect = require('chai').expect,
  assert = require('chai').assert,
  regression = require('../index'),
  csvdata = require('csvdata');

let df1;

const DataFrame = require("dataframe-js").DataFrame;
/**
 *Comments here
 */
describe('simple_regression_example', function () {

  it('create_model', function (done) {
    csvdata.load('./data/mpg.csv').then(df => {
      df1 = new DataFrame(df);
      df1 = df1.select('cyl', 'mpg', 'disp', 'hp', 'wt', 'acc', 'year');
      done();
    })
  });

  it('validate_missing_tilde', function (done) {

    try {
      regression.lm('mpg .', df1);
    } catch (e) {
      expect(e.message).to.equal("Invalid formula - missing '~'");
      done();
    }

  });


  it('validate_invalid_target', function (done) {

    try {
      regression.lm('aaa ~ .', df1);
    } catch (e) {
      expect(e.message).to.equal("Invalid target in formula");
      done();
    }

  });

  it('validate_missing_target', function (done) {

    try {
      regression.lm('   ~ .', df1);
    } catch (e) {
      expect(e.message).to.equal("Missing target in formula");
      done();
    }

  });

  it('validate_missing_predictors', function (done) {

    try {
      regression.lm('mpg ~ ', df1);
    } catch (e) {
      expect(e.message).to.equal("Missing predictors in formula");
      done();
    }

  });

  it('validate_invalid_predictors1', function (done) {

    try {
      regression.lm('mpg  ~ hp + cyyl ', df1);
    } catch (e) {
      expect(e.message).to.equal("Invalid term in formula: cyyl");
      done();
    }

  });

  it('validate_invalid_predictors2', function (done) {

    try {
      regression.lm('mpg  ~ hp + *cyl ', df1);
    } catch (e) {
      expect(e.message).to.equal("Invalid term in formula: ");
      done();
    }

  });

  it('validate_invalid_predictors3', function (done) {

    try {
      regression.lm('mpg  ~ hp + + cyl ', df1);
    } catch (e) {
      expect(e.message).to.equal("Invalid term in formula: ");
      done();
    }

  });

  it('validate_invalid_predictors4', function (done) {

    try {
      regression.lm('mpg  ~ hp + cyyl*cyl ', df1);
    } catch (e) {
      expect(e.message).to.equal("Invalid term in formula: cyyl");
      done();
    }

  });

  it('validate_invalid_predictors5', function (done) {

    try {
      regression.lm('mpg  ~ hp + cyyl:cyl ', df1);
    } catch (e) {
      expect(e.message).to.equal("Invalid term in formula: cyyl");
      done();
    }

  });
});
