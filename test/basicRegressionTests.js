const expect = require('chai').expect,
   regression = require('../index.js'),
   csvdata = require('csvdata');


const DataFrame = require("dataframe-js").DataFrame;
/**
 *Comments here
 */
describe('simple_regression_example', function () {

  it('create_model_with_na_values', function (done) {
    csvdata.load('./data/Davis.csv').then(df => {
      let df1 = new DataFrame(df);
      this.model = regression.lm('repwt ~ height + weight + repht', df1);
      expect(this.model.r_squared).to.equal(0.9626040946067305);
      done();
    })
  });

  it('test_model_with_invalid_columns', function (done) {
    csvdata.load('./data/Davis.csv').then(df => {
      let df1 = new DataFrame(df);
      this.model = regression.lm('repwt ~ .', df1, {dropInvalidColumns:true});
      expect(this.model.summary.coefficients.dim()[0]).to.equal(5);
      expect(this.model.summary.coefficients.dim()[1]).to.equal(5);
      done();
    })
  });


  // If automating factors, the sexM coefficient should be used.
  it('test_model_with_non_numeric_values_automate_factors', function (done) {
    let options = {
      automateFactors: true
    };
    csvdata.load('./data/Davis.csv').then(df => {
      let df1 = new DataFrame(df);
      this.model = regression.lm('repwt ~ .', df1, options);
      expect(this.model.summary.coefficients.mat[2][0]).to.equal('sexM');
      done();
    })
  });

  it('create_model', function (done) {
    csvdata.load('./data/mpg.csv').then(df => {
      let df1 = new DataFrame(df);
      this.model = regression.lm('mpg ~ cyl + wt', df1);
      done();
    })
  });

  //CJ
  it('create_model', function (done) {
    csvdata.load('./data/mpg.csv').then(df => {
      this.model = regression.lm('mpg ~ cyl + wt', df);
      let summary = this.model.summary;
      console.log("R squared: " + summary.r_squared);
      console.log("Adj R squared: " + summary.adj_r_squared);
      console.log("F: " + summary.F);
      console.log("Degrees of freedom: " + summary.degFreedom);
      console.log("Residual standard error: " + summary.sigma);
      console.log("\n\n");
      console.log("Coefficients can be accessed as a dictionary of arrays");
      console.log(summary.coefficients.toDict());
      console.log("... or elements of a matrix");
      console.log(summary.coefficients.mat);
      console.log("\n\n");
      console.log("Same as residual statistics");
      console.log(summary.residuals.toDict());
      done();
    })
  });


  it('test_model_summary', function (done) {
    let summary = this.model.summary;
    expect(summary).to.not.be.null;
    console.log(summary.toString());
    done();
  });

  it('test_model_summary_call', function (done) {
    let summary = this.model.summary;
    expect(summary.call.toString()).to.be.equal('lm(mpg ~ cyl + wt)');
    done();
  });

  it('test_model_summary_coefficients', function (done) {
    let coef = this.model.summary.coefficients;
    expect(coef).to.not.be.null;
    expect(coef.mat).to.not.be.null;
    expect(coef.mat[1][0]).to.equal('cyl');
    expect(coef.dim()[0]).to.equal(3);
    expect(coef.dim()[1]).to.equal(5);
    expect(coef.mat[1][1]).to.equal(-0.7192694685962204);
    //console.log(this.model.summary.coefficients.toString());
    done();
  });

  it('test_model_summary_residuals', function (done) {
    let res = this.model.summary.residuals;
    expect(res).to.not.be.null;
    expect(res.mat).to.not.be.null;
    expect(res.dim()[0]).to.equal(1);
    expect(res.dim()[1]).to.equal(5);
    expect(res.mat[0][0]).to.equal(-12.638995549351037);
    expect(res.mat[0][1]).to.equal(-2.8816383979474907);
    expect(res.mat[0][2]).to.equal(-0.28836806118915526);
    expect(res.mat[0][3]).to.equal(2.195003911205035);

    //console.log(this.model.summary.coefficients.toString());
    done();
  });

  it('test_model_coefficients', function (done) {

    let coef = this.model.coefficients;
    expect(coef).to.not.be.null;
    expect(coef.mat).to.not.be.null;
    expect(coef['(inter.)']).to.equal(46.27984366893338);
    expect(coef['cyl']).to.equal(-0.7192694685962204);
    done();
  });

  it('test_model_residuals', function (done) {

    let res = this.model.residuals;
    expect(res).to.not.be.null;
    expect(res[0]).to.equal(-0.28248116068477813);
    done();
  });

  it('test_model_fitted_values', function (done) {

    let fitted_values = this.model.fittedValues;
    expect(fitted_values).to.not.equal(undefined);
    expect(fitted_values[0]).to.equal(18.282481160684778);
    done();
  });

  it('test_model_call', function (done) {
    expect(this.model.call).to.be.equal('lm(mpg ~ cyl + wt)');
    done();
  });

  it('test_model_r_squared', function (done) {

    let r_squared = this.model.r_squared;
    expect(r_squared).to.not.equal(undefined);
    expect(r_squared).to.be.equal(0.6968669371610983);
    done();
  });

  it('test_model_adj_r_squared', function (done) {

    let adj_r_squared = this.model.adj_r_squared;
    expect(adj_r_squared).to.not.equal(undefined);
    expect(adj_r_squared).to.be.equal(0.6953003580249799);

    done();
  });

  it('test_model_summary_sigma', function (done) {

    let sigma = this.model.summary.sigma;
    expect(sigma).to.not.equal(undefined);
    expect(sigma).to.be.equal(4.314193982299181);
    done();
  });

  it('test_model_summary_fstatistic', function (done) {

    let fstats = this.model.fstatistics;
    //expect(fstats).to.not.equal(undefined);

    done();
  });

  it('test_predict_basic', function (done) {

    const newValues = new DataFrame([
      {cyl: 8, wt: 3500},
      {cyl: 6, wt: 2000}
    ], ['cyl', 'wt']);

    let fit = this.model.predict(newValues);
    expect(fit).to.not.equal(undefined);
    expect(fit[0]).to.be.equal(18.307872949222997);
    expect(fit[1]).to.be.equal(29.268332588247134);
    done();
  });

  it('test_predict_basic', function (done) {

    const newValues = new DataFrame([
      {cyl: 8, wt: 3500},
      {cyl: 6, wt: 2000}
    ], ['cyl', 'wt']);

    let fit = this.model.predict(newValues);
    expect(fit).to.not.equal(undefined);
    expect(fit[0]).to.be.equal(18.307872949222997);
    expect(fit[1]).to.be.equal(29.268332588247134);
    done();
  });

  it('test_predict_confidence', function (done) {

    const newValues = new DataFrame([
      {cyl: 8, wt: 3500},
      {cyl: 6, wt: 2000},
      {cyl: 6, wt: 3500},
      {cyl: 4, wt: 3500},
      {cyl: 2, wt: 3500}
    ], ['cyl', 'wt']);

    let fit = this.model.predict(newValues, 'confidence', .99);
    //fit.show();
    expect(fit).to.not.equal(undefined);
    expect(fit.mat[0][0]).to.be.equal(18.307872949222997);
    expect(fit.mat[0][1]).to.be.equal(16.944478752252614);
    expect(fit.mat[0][2]).to.be.equal(19.67126714619338);
    expect(fit.mat[1][0]).to.be.equal(29.268332588247134);
    expect(fit.mat[1][1]).to.be.equal(27.345352164898884);
    expect(fit.mat[1][2]).to.be.equal(31.191313011595383);

    done();
  });

  it('test_predict_prediction', function (done) {

    const newValues = new DataFrame([
      {cyl: 8, wt: 3500},
      {cyl: 6, wt: 2000},
      {cyl: 6, wt: 3500},
      {cyl: 4, wt: 3500},
      {cyl: 2, wt: 3500}
    ], ['cyl', 'wt']);

    let fit = this.model.predict(newValues, 'prediction', .99);
    //fit.show();
    expect(fit).to.not.equal(undefined);
    expect(fit.mat[0][0]).to.be.equal(18.307872949222997);
    expect(fit.mat[0][1]).to.be.equal(7.057265280952393);
    expect(fit.mat[0][2]).to.be.equal(29.5584806174936);
    expect(fit.mat[1][0]).to.be.equal(29.268332588247134);
    expect(fit.mat[1][1]).to.be.equal(17.936290279925238);
    expect(fit.mat[1][2]).to.be.equal(40.60037489656903);

    done();
  });


  it('create_model_with_duplicates', function (done) {
    csvdata.load('./data/mpg.csv').then(df => {
      let df1 = new DataFrame(df);
      this.model = regression.lm('mpg ~ cyl + wt + cyl + wt', df1);
      done();
    })
  });

  it('test_model__with_duplicates_summary', function (done) {
    let summary = this.model.summary;
    expect(summary).to.not.be.null;
    console.log(summary.toString());
    done();
  });


  // We should check that the duplicate are removed
  it('test_model_with_duplicates_summary_coefficients', function (done) {
    let coef = this.model.summary.coefficients;
    expect(coef).to.not.be.null;
    expect(coef.mat).to.not.be.null;
    expect(coef.mat[1][0]).to.equal('cyl');
    expect(coef.dim()[0]).to.equal(3);
    expect(coef.dim()[1]).to.equal(5);
    expect(coef.mat[1][1]).to.equal(-0.7192694685962204);
    done();
  });


  it('create_model_with_substracted_terms', function (done) {
    csvdata.load('./data/mpg.csv').then(df => {
      let df1 = new DataFrame(df);
      this.model = regression.lm('mpg ~ cyl * wt - cyl', df1);
      done();
    })
  });

  it('test_model_with_substracted_terms', function (done) {
    let summary = this.model.summary;
    expect(summary).to.not.be.null;
    let coef = this.model.summary.coefficients;
    expect(coef.dim()[0]).to.equal(3);
    expect(coef.dim()[1]).to.equal(5);
    expect(coef.mat[1][0]).to.equal('wt');
    expect(coef.mat[2][0]).to.equal('cyl:wt');
    expect(coef.mat[3]).to.equal(undefined);
    console.log(summary.toString());
    done();
  });


  it('create_model_with_substracted_terms', function (done) {
    csvdata.load('./data/mpg.csv').then(df => {
      let df1 = new DataFrame(df);
      this.model = regression.lm('mpg ~ cyl*wt*hp - cyl - cyl:hp ', df1);
      done();
    })
  });

  it('test_model_with_substracted_terms', function (done) {
    let summary = this.model.summary;
    expect(summary).to.not.be.null;
    let coef = this.model.summary.coefficients;
    expect(coef.dim()[0]).to.equal(5);
    expect(coef.dim()[1]).to.equal(5);
    expect(coef.mat[1][0]).to.equal('wt');
    expect(coef.mat[2][0]).to.equal('hp');
    expect(coef.mat[3][0]).to.equal('cyl:wt');
    expect(coef.mat[4][0]).to.equal('wt:hp');
    expect(coef.mat[5]).to.equal(undefined);
    console.log(summary.toString());
    done();
  });

  it('create_model', function (done) {
    csvdata.load('./data/mpg.csv').then(df => {
      let df1 = new DataFrame(df);
      df1 = df1.select('cyl', 'mpg', 'disp', 'hp', 'wt', 'acc', 'year');
      this.model = regression.lm('mpg ~ .', df1);
      let summary = this.model.summary;
      console.log(summary.toString());
      expect(summary).to.not.be.null;
      console.log(summary.toString(8));
      done();
    })
  });
});
