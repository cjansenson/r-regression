const expect = require('chai').expect,
  assert = require('chai').assert,
  regression = require('../index'),
  csvdata = require('csvdata');


const DataFrame = require("dataframe-js").DataFrame;
/**
 *Comments here
 */
describe('factor_interaction', function () {
  // beforeEach(function () {
  //   // DO something?
  // });
  it('create_factor_factor_interaction_model', function (done) {
    csvdata.load('./data/epa2015.csv').then(df => {
      let df1 = new DataFrame(df);
      let options = {
        factors: ['type', 'drive', 'lockup']
      };
      this.model = regression.lm('CO ~ weight + weight:nvratio', df1, options);
      //this.model = regression.lm('CO ~ type:drive', df1, options);
      //this.model = regression.lm('CO ~ weight:drive', df1, options);
      //this.model = regression.lm('CO ~ weight:nvratio', df1, options);
      done();
    })
  });

  it('interaction_basic_model_coefficients', function (done) {
    let summary = this.model.summary;
    expect(summary).to.not.be.null;
    console.log(summary.toString(10));
    expect(this.model.summary.sigma).to.equal(0.4694079614734085);
    expect(this.model.summary.degFreedom).to.equal(4408);
    done();
  });


  it('test_model_summary_coefficients', function (done) {
    // let summary = this.model.summary;
    // expect(summary.call.toString()).to.be.equal('lm(mpg ~ cyl + wt)');
    // done();
    //expect(this.model.summary.coefficients).to.not.be.null;
    let coef = this.model.summary.coefficients;
    expect(coef).to.not.be.null;
    expect(coef.mat).to.not.be.null;
    expect(coef.mat[1][0]).to.equal('weight');
    expect(coef.mat[2][0]).to.equal('weight:nvratio');
    expect(coef.dim()[0]).to.equal(3);
    expect(coef.dim()[1]).to.equal(5);
    expect(coef.mat[0][1]).to.equal(0.013632248075330838);
    expect(coef.mat[0][2]).to.equal(0.039918405465864844);
    expect(coef.mat[1][1]).to.equal(0.000020527056631192915);
    expect(coef.mat[1][2]).to.equal(0.000009333867616764644);
    //console.log(this.model.summary.coefficients.toString());
    done();
  });

  it('test_model_call', function (done) {
    expect(this.model.call).to.be.equal('lm(CO ~ weight + weight:nvratio)');
    done();
  });

});
