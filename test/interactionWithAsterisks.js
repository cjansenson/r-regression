const expect = require('chai').expect,
  assert = require('chai').assert,
  regression = require('../index'),
  csvdata = require('csvdata');


const DataFrame = require("dataframe-js").DataFrame;
/**
 *Comments here
 */
describe('factor_factor_interaction_example', function () {

  it('create_asterisk_interaction_model2', function (done) {
    this.timeout(10000);
    csvdata.load('./data/epa2015.csv').then(df => {
      let df1 = new DataFrame(df);
      let options = {
        factors: ['type', 'drive', 'lockup']
      };

      //horse + horse:weight + horse:weight:lockup + horse:lockup + weight + weight:lockup + lockup + type
      this.model = regression.lm('CO ~ horse*weight*lockup + type', df1, options);

      done();
    })
  });

  it('asterisk_interactive_model_coefficients', function (done) {
    let summary = this.model.summary;
    console.log(summary.toString(5, [10]));
    let coef = summary.coefficients;
    expect(summary).to.not.be.null;
    expect(summary.sigma).to.equal(0.46478821902055295);
    expect(summary.degFreedom).to.equal(4401);
    expect(coef.dim()[0]).to.equal(10);
    expect(coef.dim()[1]).to.equal(5);
    expect(summary.r_squared).to.be.equal(0.04071769934403091);
    done();
  });

  it('create_complex_interaction_model2', function (done) {
    this.timeout(10000);
    csvdata.load('./data/epa2015.csv').then(df => {
      let df1 = new DataFrame(df);
      let options = {
        factors: ['type', 'drive', 'lockup']
      };
      this.model = regression.lm('CO ~  type:lockup + type:drive + lockup:drive', df1, options);
      done();
    })
  });


  it('interaction_plus_interaction_model_coefficients2', function (done) {
    let summary = this.model.summary;
    let coef = summary.coefficients;
    expect(summary).to.not.be.null;
    expect(summary.sigma).to.equal(0.469394431579826);
    expect(summary.degFreedom).to.equal(4390);
    expect(coef.dim()[0]).to.equal(21);
    expect(coef.dim()[1]).to.equal(5);
    expect(summary.r_squared).to.be.equal(0.024055262016528522);
    done();
  });

});
